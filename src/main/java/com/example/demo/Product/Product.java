package com.example.demo.Product;

public class Product
{
    private long ProductId ;
    private String ProductName;
    private String ProductDesc;
    private int ProductPrice;

    public Product(long productId) {
        ProductId = productId;
    }

    public Product(long productId, String productName) {
        ProductId = productId;
        ProductName = productName;
    }

    public Product(long productId, String productName, String productDesc, int productPrice) {
        ProductId = productId;
        ProductName = productName;
        ProductDesc = productDesc;
        ProductPrice = productPrice;
    }

    public long getProductId() {
        return ProductId;
    }

    public void setProductId(long productId) {
        ProductId = productId;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductDesc() {
        return ProductDesc;
    }

    public void setProductDesc(String productDesc) {
        ProductDesc = productDesc;
    }

    public int getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(int productPrice) {
        ProductPrice = productPrice;
    }
}
