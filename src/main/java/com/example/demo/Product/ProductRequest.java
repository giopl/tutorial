package com.example.demo.Product;

public class ProductRequest {

    private String name;
    private String description;
    private int price;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }
}
