package com.example.demo.Product;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductService {

private ProductDao productDao;

    public ProductService(ProductDao productDao) {
        this.productDao = productDao;
    }

    public List<Product> fetchProducts()
    {
        return  productDao.getAllProducts();
    }

    public Product fetchProductById(Long id) {
        return productDao.getProductById(id);
    }


    public int updateProduct(Product product) {
        return productDao.updateProduct(product);
    }


    public int removeProduct(long id) {
        return productDao.deleteProduct(id);
    }


    public Product addProduct(ProductRequest product){
        return productDao.createProduct(product);
    }

}
