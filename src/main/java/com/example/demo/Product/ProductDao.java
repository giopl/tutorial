package com.example.demo.Product;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.net.PortUnreachableException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class ProductDao {

    private DataSource dataSource;

    @Autowired
    public ProductDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Product> getAllProducts() {
        try {
            List<Product> products = new JdbcTemplate(this.dataSource).query(
                    "SELECT id, name, description, price from product",
                    (rs, rowNum) -> new Product(
                            rs.getLong("id"),
                            rs.getString("name"),
                            rs.getString("description"),
                            rs.getInt("price")

                    )
            );
            return products;

        } catch (Exception e) {
            return null;
        }
    }

    public Product getProductById(long id) {
        try {
            List<Product> product = new JdbcTemplate(this.dataSource).query(
                    "SELECT id, name, description, price from product where id= ?",
                    new Object[]{id},
                    (rs, rowNum) -> new Product(
                            rs.getLong("id"),
                            rs.getString("name"),
                            rs.getString("description"),
                            rs.getInt("price")
                    )
            );
            return product.get(0);

        } catch (Exception e) {
            return null;
        }
    }

    public int updateProduct(Product product) {
        try {
            int query = new JdbcTemplate(this.dataSource).update(
                    "UPDATE product set name=?, description=?, price=?  where id= ?",
                    new Object[]{product.getProductName(), product.getProductDesc(), product.getProductPrice(), product.getProductId()}
                    );
            return query;

        } catch (Exception e) {
            return 0;
        }
    }

    public Product createProduct (ProductRequest product) {
        try {
            SimpleJdbcInsert insert = new SimpleJdbcInsert(this.dataSource)
                    .withTableName("product")
                    .usingGeneratedKeyColumns("id");


            HashMap parameters = new HashMap<String, Object>(1);
            parameters.put("name", product.getName());
            parameters.put("description", product.getDescription());
            parameters.put("price", product.getPrice());
            long id = insert.executeAndReturnKey(parameters).longValue();
            return new Product(id);

        } catch (Exception e) {
            return null;
        }
    }


    public int deleteProduct(long id) {
     try {
        int result = new JdbcTemplate(this.dataSource).update(
                "DELETE FROM product WHERE id = ?",
                new Object[]{ id}
        );
        return result;
    } catch (Exception e) {
        return 0;
    }
    }


}
