package com.example.demo.Product;

import ch.qos.logback.core.pattern.util.RegularEscapeUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/products")
public class ProductController {

    private ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping()
    public ResponseEntity getProducts() {

        List<Product> products = productService.fetchProducts();
        if(products.size() > 0)
        {
            return ResponseEntity.ok(products);
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @PostMapping
    public ResponseEntity createProduct(@RequestBody ProductRequest product) {

        Product created = productService.addProduct(product);
        if(created != null )
        {
            return ResponseEntity.created(URI.create("/products")).build();
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }




    @PutMapping(value = "/{id}" )
    public ResponseEntity updateProduct(@RequestBody ProductRequest request, @PathVariable Long id) {

        Product product = new Product(id, request.getName(), request.getDescription(), request.getPrice());

        if (productService.updateProduct(product)== 1)
        {
            return ResponseEntity.ok("updated");
        }
        
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }



}
